<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/* This code is defining routes for a web application of emails. */

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/v1/inbox', 'App\Http\Controllers\EmailController@index');
Route::post('/v1/messages', 'App\Http\Controllers\EmailController@store');
Route::delete('/v1/messages/{id}', 'App\Http\Controllers\EmailController@destroy');
Route::put('/v1/messages/{id}', 'App\Http\Controllers\EmailController@update');
