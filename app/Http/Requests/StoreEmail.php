<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmail extends FormRequest
{
    /**
     * This is a PHP function that authorizes and validates an email message with
     * specific rules and attributes.
     * 
     * @return bool The code is returning three methods: `authorize()`, `rules()`,
     * and `attributes()`.
     */
    
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'to_email' => 'required|email',
            'subject' => 'required|string|max:255',
            'content' => 'required|string',
        ];
    }

     public function attributes()
     {
        return [
            'to_email' => 'para',
            'subject' => 'asunto',
            'content' => 'contenido',
             
        ];
     }
}
