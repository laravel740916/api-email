<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEmail;
use App\Models\Email;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    /**
     * These are PHP functions for handling CRUD operations on emails, including
     * retrieving, creating, updating, and deleting emails.
     * 
     * @return The code is implementing a RESTful API for managing emails.
     */
    
    public function index()
    {
        $emails = Email::orderBy('created_at', 'desc')->paginate(10);
        return response()->json($emails);
    }

    public function store(StoreEmail $request)
    {   
        $email = new Email;
        $email->to_email = $request->to_email;
        $email->subject = $request->subject;
        $email->content = $request->content;
        $email->save();
        return response()->json($email);
    }

    public function destroy($id)
    {
        $email = Email::find($id);
        if (!$email) {
            return response()->json(['message' => 'Correo no encontrado'], 404);
        }
        $email->delete();
        return response()->json(['message' => 'Correo eliminado'], 200);
    }

    public function update(Request $request, $id)
    {
        $email = Email::find($id);
        if (!$email) {
            return response()->json(['message' => 'Correo no encontrado'], 404);
        }
        $email->to_email = $request->to_email;
        $email->subject = $request->subject;
        $email->content = $request->content;
        $email->save();
        return response()->json($email);
    }
}
