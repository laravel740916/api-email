<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

-   [Simple, fast routing engine](https://laravel.com/docs/routing).
-   [Powerful dependency injection container](https://laravel.com/docs/container).
-   Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
-   Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
-   Database agnostic [schema migrations](https://laravel.com/docs/migrations).
-   [Robust background job processing](https://laravel.com/docs/queues).
-   [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 2000 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

-   **[Vehikl](https://vehikl.com/)**
-   **[Tighten Co.](https://tighten.co)**
-   **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
-   **[64 Robots](https://64robots.com)**
-   **[Cubet Techno Labs](https://cubettech.com)**
-   **[Cyber-Duck](https://cyber-duck.co.uk)**
-   **[Many](https://www.many.co.uk)**
-   **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
-   **[DevSquad](https://devsquad.com)**
-   **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
-   **[OP.GG](https://op.gg)**
-   **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
-   **[Lendio](https://lendio.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Documentación de API

## Introducción

Esta es la documentación de la API de administracion de emails personales. La API permite realizar operaciones CRUD sobre las tareas.

### El archivo .env en el directorio raíz del proyecto para conectarse a la base de datos. Debe contener:

DB_CONNECTION=mysql
DB_HOST=db-mysql-nyc1-46579-do-user-13949287-0.b.db.ondigitalocean.com
DB_PORT=25060
DB_DATABASE=defaultdb
DB_USERNAME=doadmin
DB_PASSWORD=AVNS_LuboP9HbCX1RBGhclyK

## Base URL

La URL base de la API es `http://127.0.0.1:8000/api`.

## Endpoints

A continuación se detallan los endpoints disponibles.

### Obtener todos los emails

### `GET  /v1/inbox `

Obtiene todos los correos existentes en la base de datos.

#### Parámetros

-   Ninguno

#### Respuesta

-   `200 OK` en caso de éxito.
-   `404 Not Found` en caso de error.

##### Ejemplo de respuesta exitosa

{
"current_page": 1,
"data": [
{
"id": 8,
"to_email": "equipos@tualiado.com",
"subject": "Compra de equipo",
"content": "Equipo especializado para instrumentos de medicina",
"created_at": "2023-04-20T12:35:20.000000Z",
"updated_at": "2023-04-20T12:35:20.000000Z"
},

    ],
    "first_page_url": "http://127.0.0.1:8000/api/v1/inbox?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http://127.0.0.1:8000/api/v1/inbox?page=1",
    "links": [
        {
            "url": null,
            "label": "&laquo; Previous",
            "active": false
        },
        {
            "url": "http://127.0.0.1:8000/api/v1/inbox?page=1",
            "label": "1",
            "active": true
        },
        {
            "url": null,
            "label": "Next &raquo;",
            "active": false
        }
    ],
    "next_page_url": null,
    "path": "http://127.0.0.1:8000/api/v1/inbox",
    "per_page": 10,
    "prev_page_url": null,
    "to": 6,
    "total": 6

}

### Crear un nuevo email

### `POST /v1/messages`

Crea una nuevo email con las propiedades recibidas.

#### Parámetros

-   `to_email` (obligatorio): Direccion de email al que sra enviado.
-   `subject`(obligatorio): Asunto del email.
-   `content` (obligatorio): Contenido del email.

##### Ejemplo de una solicitud exitosa

{
"to_email": "facturacion@coffesky.com",
"subject": "Favor de facturar ticket",
"content": "Facturar ticket con los datos proporcionados anterioormente",
"updated_at": "2023-04-20T14:56:55.000000Z",
"created_at": "2023-04-20T14:56:55.000000Z",
"id": 9
}

#### Respuesta

-   `201 Created` en caso de éxito.
-   `400 Bad Request` en caso de error.

### Modificar un email.

### `PUT /v1/messages/{{id}}`

Modificar un email existente dado un ID con las propiedades obtenidas en el body (JSON).

#### Parámetros

-   `to_email` (obligatorio): Direccion de email al que sra enviado.
-   `subject`(obligatorio): Asunto del email.
-   `content` (obligatorio): Contenido del email.

##### Ejemplo de una solicitud exitosa

#### Respuesta

-   `200 OK` en caso de éxito.
-   `400 Bad Request` en caso de error.

### Eliminar un email existente

### `DELETE /v1/messages/{{id}}`

Eliminar una tarea existente dado un ID.

#### Parámetros

-   Ninguno

#### Respuesta

-   `200 OK` en caso de éxito.
-   `404 Not Found` en caso de error.

##### Ejemplo de respuesta exitosa

{
"message": "Correo eliminado"
}
# laravel-api-email
